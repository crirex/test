#pragma once
#include<vector>
#include"Card.h"
#include<random>

namespace Game
{
	const size_t deckSize = 81;
	const Color Colors[possibilities] = { Red,Green,Blue };
	const Number Numbers[possibilities] = { Number::One,Number::Two,Number::Three };
	const Symbol Symbols[possibilities] = { Symbol::Diamond,Symbol::Squiggle,Symbol::Oval };
	const Shading Shadings[possibilities] = { Shading::Solid,Shading::Striped,Shading::Open };

	class Deck
	{
	private:
		size_t currentDeckPoistion;
		std::vector<Card> _Cards;
		void createNewDeck();
	public:
		Deck();
		~Deck();
		void Shuffle();
		Card nextCard();
	};
}

