#pragma once
#include"Color.h"

namespace Game
{
	const size_t possibilities = 3;
	const size_t maxColor = 255;
	enum Number
	{

		One,
		Two,
		Three
	};
	enum Symbol
	{
		Diamond,
		Squiggle,
		Oval
	};
	enum Shading
	{
		Solid,
		Striped,
		Open
	};
	const Color Red(maxColor, 0, 0);
	const Color Green(0, maxColor, 0);
	const Color Blue(0, 0, maxColor);
	

	class Card
	{
	private:
		Number _number;
		Symbol _symbol;
		Shading _shading;
		Color _color;
	public:
		static size_t numberOfCardsCreated;
		Card(const Number& number, const Symbol& symbol, const Shading& shading, const Color& color);
		Card(const Card& card);
		~Card();

		Card& operator= (const Card& card);

	};
}

