#pragma once
#include"Deck.h"
#include"Board.h"
#include"Card.h"

namespace Game
{
	class Dealer
	{
	public:
		void putUpBoard(Deck& deck,Board& board) const;
		Dealer();
		~Dealer();
	};
}

