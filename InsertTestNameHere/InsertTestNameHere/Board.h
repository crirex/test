#pragma once
#include<vector>
#include"Card.h"

namespace Game
{
	class Board
	{
	private:
		size_t _boardSize;
	public:
		std::vector<Card> _Cards;
		Board(size_t boardSize = 12);
		~Board();
		size_t GetBoardSize();
	};

}