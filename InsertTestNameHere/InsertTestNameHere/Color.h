#pragma once
#include"Card.h"
#include<random>

namespace Game
{
	class Color
	{
	private:
		unsigned char _red;
		unsigned char _green;
		unsigned char _blue;
	public:
		Color(const unsigned char& red = 0, const unsigned char& green = 0, const unsigned char& blue = 0);
		Color(const Color& color);
		~Color();
		void generateRandomColor();
	};
}

