#include "Dealer.h"

namespace Game
{
	Dealer::Dealer()
	{
	}


	Dealer::~Dealer()
	{
	}

	void Dealer::putUpBoard(Deck& deck, Board& board) const
	{
		for (size_t i = 0; i < board.GetBoardSize(); ++i)
		{
			board._Cards.push_back(deck.nextCard());
		}
	}

}