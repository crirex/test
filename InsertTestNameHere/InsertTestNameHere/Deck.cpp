#include "Deck.h"

namespace Game
{
	void Deck::createNewDeck()
	{
		for (const Number& number : Numbers)
		{
			for (const Symbol& symbol : Symbols)
			{
				for (const Shading& shading : Shadings)
				{
					for (const Color& color : Colors)
					{
						_Cards.push_back(Card(number, symbol, shading, color));
					}
				}
			}
		}
	}

	Deck::Deck() : currentDeckPoistion(0)
	{
		createNewDeck();
	}

	Deck::~Deck()
	{

	}

	void Deck::Shuffle()
	{
		std::mt19937 rng;
		rng.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> randomNumber(NULL, deckSize - 1);

		for (auto& card : _Cards)
		{
			std::swap(card, _Cards[randomNumber(rng)]);
		}
	}
	Card Deck::nextCard()
	{
		Card next = _Cards[currentDeckPoistion++];
		if (currentDeckPoistion == deckSize)
		{
			createNewDeck();
		}
		return next;
	}
}
