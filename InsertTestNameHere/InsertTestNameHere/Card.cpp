#include "Card.h"

namespace Game
{
	Card::Card(const Number& number, const Symbol& symbol, const Shading& shading, const Color& color) 
		: _number(number), 
		_symbol(symbol),
		_shading(shading), 
		_color(color)
	{
		numberOfCardsCreated++;
	}


	Card::~Card()
	{

	}

	Card::Card(const Card& card)
	{
		*this = card;
	}

	Card& Card::operator= (const Card& card)
	{
		_color = card._color;
		_number = card._number;
		_shading = card._shading;
		_symbol = card._symbol;
		return *this;
	}

}