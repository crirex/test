#include "Color.h"

namespace Game
{
	Color::Color(const unsigned char& red, const unsigned char& green, const unsigned char& blue)
		: _red(red),
		_green(green),
		_blue(blue)
	{

	}

	Color::Color(const Color& color)
		: _red(color._red),
		_green(color._green),
		_blue(color._blue)
	{

	}

	Color::~Color()
	{

	}

	void Color::generateRandomColor()
	{
		std::mt19937 rng;
		rng.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> randomNumber(NULL, 255);

		*this = Color(randomNumber(rng), randomNumber(rng), randomNumber(rng));
	}
}

