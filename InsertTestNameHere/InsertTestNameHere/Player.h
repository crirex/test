#pragma once
#include"Card.h"
#include<iostream>
#include"Board.h"

namespace Game
{
	class Player
	{
		Card* pair[3];
	public:
		Player();
		~Player();
		void selectCards(Board& board);
	};

}